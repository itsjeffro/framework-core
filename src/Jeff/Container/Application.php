<?php

namespace Jeff\Container;

use Exception;

class Application extends Container
{
    /**
     * @param array
     */
    protected $config = [];
    
    /**
     * Apllication base path being run.
     * @param string
     */
    public $basePath;

    /**
     * Description.
     *
     * @arrat  string $config
     * @return void
     */
    public function __construct($config)
    {
        $this->config = $config;

        date_default_timezone_set(
            $this->getConfig('timezone')
         );
         
         $this->basePath = $this->getConfig('base_path');
    }
    
    /**
     * Description.
     * 
     * @param  string $key
     * @return mixed
     */
    protected function getConfig($key)
    {
        if (array_key_exists($key, $this->config)) {
            return $this->config[$key];
        }
        
        throw new Exception('Offset in config not defined.');
    }
}
