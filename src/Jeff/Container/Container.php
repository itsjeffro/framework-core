<?php
namespace Jeff\Container;

use Exception;
use ReflectionClass;
use ReflectionParameter;

class Container
{
    private static $instance;
   
   
    /**
     * Description.
     */
    public static function getInstance()
    {
         return static::$instance;
    }

    /**
     * Description.
     */
    public function build($concrete, $parameters = [])
    {		
        $reflector = new ReflectionClass($concrete);        
    
        $constructor = $reflector->getConstructor();
    
        if (is_null($constructor)) {
            return new $concrete;
        }
        
        $dependencies = $constructor->getParameters(); 
        
        $parameters = $this->keyParametersByArgument(
            $dependencies, $parameters
        );
        
        //print_r($parameters);
    
        $instances = $this->getDependencies(
            $dependencies, $parameters
        );
    
        return $reflector->newInstanceArgs($instances);
    }

    /**
     * Description.
     */
    protected function getDependencies(array $parameters, array $primitives = [])
    {
        $dependencies = [];
    
        foreach ($parameters as $parameter) {
            $dependency = $parameter->getClass();
            
            if (array_key_exists($parameter->name, $primitives)) {
                $dependencies[] = $primitives[$parameter->name];
            } 
            else if (is_null($dependency)) {
                $dependencies[] = $this->resolveNonClass($parameter);
            }
            else {
                $dependencies[] = new $dependency->name;
            }
         }
         
         //var_dump($dependencies);
    
         return (array) $dependencies;
    }
    
    /**
     * Check for a default argument since one was not passed through class previously.
     * 
     * @param  ReflectionParameter $parameter
     * @return mixed
     */
    protected function resolveNonClass(ReflectionParameter $parameter)
    {
        if ($parameter->isDefaultValueAvailable()) {
            return $parameter->getDefaultValue();
        }
        
        throw new Exception('Cannot resolve passed or default argument in class.');
    }
    
    /**
     * Description.
     */
    protected function keyParametersByArgument(array $dependencies, array $parameters)
    {
        foreach ($parameters as $key => $value) {
            if (is_numeric($key)) {
                unset($parameters[$key]);

                $parameters[$dependencies[$key]->name] = $value;
            }
        }
        
        return $parameters;
    }
}